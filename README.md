# Serverless Rust Microservice with AWS Lambda and DynamoDB Integration

## Project Overview

This repository contains a simple **Serverless Rust Microservice** deployed as an **AWS Lambda function**. The service connects to a **DynamoDB** and implements a basic functionality to interact with database.

<div align="center">
  <img src="https://cdn-media-1.freecodecamp.org/images/1*DpwCOdwJh84BzmZCuJPLmA.png">
</div>


## Project Structure

- **src/main.rs**: Contains the main code for the AWS Lambda function, including request handling and response generation.
- **Cargo.toml**: Specifies project metadata and dependencies, allowing for easy dependency management and version control.

## Getting Started
1. Clone this repository to your local machine:

   ```bash
   git clone https://gitlab.com/aghakishiyeva/serverless-rust-microservice-with-aws-lambda-and-dynamodb-integration.git
   ```

2. Ensure you have Rust installed:

    ```bash
    rustc --version
    ```

3. Make sure you have the AWS CLI installed and configured with appropriate credentials.

    ```bash
    aws --version
    ```

    ```bash
    aws configure list
    ```

4. Build the Lambda function:

    ```bash
    cargo build --release
    ```

5. Deploy the Lambda function using the AWS CLI:
   
    ```bash
    aws lambda create-function --function-name your-function-name 
    --runtime provided 
    --role your-role-arn 
    --handler serverless_rust_microservice --zip-file fileb://path/to/your/built/zip --environment  
    Variables="{AWS_REGION=your-region, AWS_ACCESS_KEY_ID=your-access-key, AWS_SECRET_ACCESS_KEY=your-secret-key}"
    ```


5. Ensure the DynamoDB table exists on the AWS Management Console with the required schema.

## Documentation

The microservice provides the following API endpoints:

- `POST /users`: Create a new user record.
- `GET /users/{id}`: Retrieve a user record by ID.
- `PUT /users/{id}`: Update an existing user record.
- `DELETE /users/{id}`: Delete a user record by ID.



