use lambda_runtime::{handler_fn, Context, Error};
use serde_json::Value;
use aws_sdk_dynamodb::{Client, types::AttributeValue};
use aws_config::{load_defaults, BehaviorVersion};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct CustomEvent {
    id: String,
    message: String,
}

#[derive(Serialize, Deserialize)]
struct CustomResponse {
    message: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: Value, _: Context) -> Result<CustomResponse, Error> {
    // Corrected AWS configuration loading to use load_from_env
    let config = load_defaults(BehaviorVersion::latest()).await;
    let client = Client::new(&config);

    // Deserialize the event to the custom struct
    let custom_event: CustomEvent = serde_json::from_value(event).expect("Error parsing event");

    // Prepare the attributes for DynamoDB insertion
    let id_attr = AttributeValue::S(custom_event.id);
    let message_attr = AttributeValue::S(custom_event.message);

    // Attempt to insert the item into DynamoDB
    let put_item_output = client.put_item()
        .table_name("GunelsTable")
        .item("id", id_attr)
        .item("message", message_attr)
        .send()
        .await;

    match put_item_output {
        Ok(_) => Ok(CustomResponse { message: "Item inserted successfully".to_string() }),
        Err(e) => Err(Error::from(e.to_string())),
    }
}
